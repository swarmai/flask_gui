from app import app
from flask import render_template, safe_join, send_from_directory, request, jsonify




@app.route('/<any(css, img, js, sound, dat):folder>/<path:filename>')
def toplevel_static(folder, filename):
    filename = safe_join(folder, filename)
    cache_timeout = app.get_send_file_max_age(filename)
    return send_from_directory(app.static_folder, filename,
                               cache_timeout=cache_timeout)

@app.route('/', methods=['GET'])
def index():
    user = {'username': 'Paul'}
    return render_template('index.html', title='Home', user=user)


@app.route("/update", methods=["POST"])
def update():
    # global lat0, lng0, alt
    # alt += .2
    # lat0 += .0001
    # lng0 += .0001
    # pos0 = [lat0, lng0, alt]
    # pos1 = [38.999057, -76.946490, 40]
    # pos = {0: pos0, 2: pos1}
    pos = {}
    with open("app/static/dat/telem.tl", 'r') as f:
        for ln in f:
            ln_array = ln.split('#')
            pos[ln_array[0]] = ln_array[1:]
        f.close()    
    returner = jsonify(pos=pos)
    return returner
    
@app.route("/get_telem", methods=["POST"])
def get_telem():
    pts = request.get_json(force = True)
    printer = ""
    for pt in pts:
        printer += (str(pt["index"]) + "#" + str(pt["group"]) + "#" 
                    + str(pt["region"]) + "#" + str(pt["region_index"]) 
                    + "#" + str(pt["lat"]) + "#" + str(pt["long"]) 
                    + "#" + str(pt["alt"]) + "\n")
    with open('app/static/dat/pts_regions.pr', 'w') as filetowrite:
        filetowrite.write(printer)
    returner = jsonify(msg="OK!")
    return returner
